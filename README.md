# Samaha Lab
Our research centers around assessing and modifying preference and reinforcer efficacy.  Often this occurs in the context of assessment and treatment of severe problem behavior, early intensive behavioral intervention, training, and transition-related skills. We embrace a translational approach with attention drawn to mechanism while working toward clear clinical end-points. We tend to focus on individuals with disabilities including Autism, parents, and teachers.

## Index
* [People](#current-students)
* [Lab-Meeting](#lab-meeting)
* [Communication](#communication)
* [Resources](#resources)
* [Recent Research](#recent-research)

# People
Feel free to connect with any of the individuals below to learn more about their research and get involved.
* **Andrew L. Samaha, Ph.D., BCBA-D** [[CV]](/CVs/samahacv.pdf) - Assistant Professor
* Marlesha Bell, M.S., BCBA [[CV]](/CVs/bellcv.pdf) - Assessment and treatment of severe problem behavior
* Anthony Concepcion, M.S., BCBA [[CV]](/CVs/concepcioncv.pdf) - Functional Analysis and Conditioned Reinforcement
* Rebecca Donnelly, B.S. [[CV]](/CVs/donnellycv.pdf) - Teacher preference for social skills interventions with students with ASD
* Spencer Gauert, M.S., BCBA [[CV]](/CVs/gauertcv.pdf) - Math, Reading, and Conditioned Reinforcement
* Karie John, B.S. [[CV]](/CVs/johncv.pdf) - Preference and Functional Analysis
* Codye Manning, B.S. [[CV]](/CVs/manningcv.pdf) - Performance feedback and preference with rugby
* Bryon Miller, M.S., BCBA [[CV]](/CVs/millercv.pdf) - Physical Activity and Nutrition
* Laurel Porter, B.S. [[CV]](/CVs/portercv.pdf) - Teacher preference for training modalities and feedback
* Christopher Rushing, B.S. - Parent preference for data collection procedures
* Chelsea Schubiger, M.S. [[CV]](/CVs/schubigercv.pdf) - Assessment and treatment of severe problem behavior
* Paige Talhelm, B.S. [[CV]](/CVs/talhelmcv.pdf) - Functional Analysis and Resurgence

# Lab-Meeting
Visitors (undergraduate and graduate students interested in research, faculty, or members of the community) are always welcome to attend lab. Please reach out to a lab member or email Andrew at andrewsamaha@usf.edu so we can connect before hand.

During Summer 2019 (starting May 9th), lab meetings will be held on `Thursday mornings from 10-12 PM in MHC2502E`. Lab meeting is the primary training experience you will receive in research. All students (masters and doctoral) are expected to attend all meetings, be engaged, ask questions, provide supportive feedback, and try out ideas.



# Communication
* [Slack](https://samahalab.slack.com) - The primary mode of communication within the lab is slack. Request access if you want to get involved in research. Here are some critical channels:
    * #general - General discussion, mostly used to store notes related to lab meetings and share/discuss data outside of lab meeting
    * #expectations - Communication and discussion on expectations for students
    * #irb - Discussion and problem solving related to IRB applications
    * #grant - Conversation related to current grant preparation
    * #preproposal - Everything from selecting a topic, preparing your literature review, to writing, and proposing your thesis
    * #postproposal - All things postproposal including IRB, data collection, decision making, writing, and defending
    * #programming - Participate in our lab course on programming
    * #music - What is burning a hole in your playlist?


* [Private GitLab](https://gitlab.com/samahalab) - Internally, we use GitLab to share resources.

# Resources
#### Guides
* [General Expectations for Students](https://gitlab.com/samahalab/writing-guide/blob/master/Expectations.md)
* [Scholarly Writing](https://gitlab.com/samahalab/writing-guide)
* [Presentations](https://gitlab.com/samahalab/writing-guide/blob/master/Presentations.md)
* [Figures](https://gitlab.com/samahalab/writing-guide/blob/master/Figures.md)
* Our [Guide to All Things IRB](https://gitlab.com/samahalab/IRB)
* [Research Binder Template](https://gitlab.com/samahalab/writing-guide/blob/master/ResearchBinderTemplate.zip)

#### Data Collection / Instant Data
Instant Data is a PC-based data collection and analysis suite. It is old and no longer maintained, but the software is still available here free of charge.
* [Instant Data PC](/Resources/InstantData/instantdatapc1.4b.zip) - Data collection app for PCs.
* [Instant Analyzer](/Resources/InstantData/InstantAnalyzer3.6beta.zip) - Session summary and analysis software.
* [Instant Rely](/Resources/InstantData/Instantrely0.5.zip) - Provides various reliability calculations on Instant Data session files.
* [Instant Data .NET](/Resources/InstantData/2010.002instantdatanet.zip) - Data collection app for .NET-based handheld devices.
  * [2008.001_InstantData.exe](/Resources/InstantData/2008.001_InstantData.exe) - A stand-alone executable for .Net-based devices.

#### Reinforcement Schedules
* Observing Response Procedure - [Live Demo](http://jstream.github.io/), [Source Code](https://gitlab.com/asamaha/MultConcurrentVI) - Observing response procedures are useful in the study of conditioned reinforcement because they allow for rates of conditioned and unconditioned reinforcement to be disassociated unlike in chain schedules, in which rates of conditioned and unconditioned reinforcement covary. However, such procedures are difficult to implement in clinical settings because they require the experimenter maintain several separate timers simultaneously (e.g., transitions between components, the duration of Sd presentation, reinforcer schedules, reinforcer access time, etc). This web-app does that.  

# Recent-Research
* Andrade-Plaza, R. [Using Contingency Maps to Teach Requests for Information](/RecentResearch/inprep.Andrade-Plaza.etal.pdf)
* Bayliss, H. [Reinforcement of Variability and Implications for Creativity in Students with ASD.](/RecentResearch/inprep.Bayliss.etal.pdf)
* Carr, C. [Group Contingency Game Comparison: Examining the Role of Group Sizes](/RecentResearch/inprep.Carr.etal.pdf)
* Kim, Y. [Effects of Delay to Reinforcement on Selections for High-tech and Low-tech Leisure Items.](/RecentResearch/inprep.Kim.etal.pdf)
* Livingston, C. [Comparison of Conditioning Procedures to Condition Praise as a Reinforcer for Children with Autism](/RecentResearch/inprep.Livingston.etal.pdf)
* Lynch, E. [Evaluation of a Hierarchal Training Model for Group Home Staff](/RecentResearch/inprep.Lynch.etal.pdf)
* Meuret, B. [Effects of Video Modeling on Preference and Reinforcer Value for Toys.](/RecentResearch/inprep.Meuret.etal.pdf)
* Pena, K. [A Component Analysis of Response Interruption and Redirection for Vocal Stereotypy in Children with ASD.](/RecentResearch/inprep.Pena.pdf)
* Schubiger, C. [Effects of Release Contingencies on Bout-like Responding.](/RecentResearch/inprep.Schubiger.etal.pdf)
* Stich, J. [Evaluating Preference for and Effectiveness of Telehealth and In-Person Parent Training.](/RecentResearch/inprep.Stich.etal.pdf)
